import VueRouter from 'vue-router'
import MainPage from '../pages/MainPage.vue'
import About from '../pages/About.vue'
import Login from '../pages/Login.vue'
import notFound from '../pages/404.vue'

export default new VueRouter({
    mode: 'history',

    routes: [ {
        path: '/',
        name: 'mainpage',
        component: MainPage
    },
    {
        path: '/about',
        name: 'about',
        component: About
        
        
        
    },
    {
        path: '/login',
        name: 'login',
        component: Login
        
    },
    {
        path: '*',
        name: 'notfound',
        component: notFound
    }
        

    ]
})