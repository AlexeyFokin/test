import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import Vuelidate from 'vuelidate'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import VueRouter from 'vue-router'
import router from './router/index'
import './assets/styles.scss'

Vue.use(VueRouter)


Vue.use(Vuelidate)
Vue.use(BootstrapVue)

new Vue({
  
  render: h => h(App),
  el: '#app',
  router
})
